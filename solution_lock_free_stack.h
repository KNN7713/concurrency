#pragma once

#include <atomic>
#include <thread>


template <typename T>
class LockFreeStack {
  struct Node {
    T element;

    std::atomic<Node*> next;
    Node() : next(nullptr) {}
    Node(T& element_) : element(std::move(element_)), next(nullptr) {}
  };

public:
  explicit LockFreeStack() {}

  ~LockFreeStack() {
    Node* garbage_top = garbage;
    while (garbage_top != nullptr) {
      Node* curr_garbage = garbage_top;
      garbage_top = garbage_top->next.load();
      delete curr_garbage;
    }
    Node* top = top_;
    while (top != nullptr) {
      Node* curr_top = top;
      top = top->next.load();
      delete curr_top;
    }
  }

  void Push(T element) {
    Node* new_top = new Node(element);
    Node* curr_top = top_.load();
    new_top->next = curr_top;
    while (!top_.compare_exchange_strong(curr_top, new_top)) {
      new_top->next = curr_top;
    }
  }

  bool Pop(T& element) {
    Node* curr_top = top_.load();
    while (true) {
      if (!curr_top) {
        return false;
      }
      if (top_.compare_exchange_strong(curr_top, curr_top->next)) {
        element = curr_top->element;
        Node* curr_garbage = garbage.load();
        curr_top->next = curr_garbage;
        while (!garbage.compare_exchange_strong(curr_garbage, curr_top)) {
          curr_top->next = curr_garbage;
        }
        return true;
      }
    }
    return false;
  }


private:
  std::atomic<Node*> top_{ nullptr };
  std::atomic<Node*> garbage{ nullptr };
};


template <typename T>
using ConcurrentStack = LockFreeStack<T>;