#pragma once

#include <queue>
#include <mutex>
#include <condition_variable>
#include <atomic>

template <class T, class Container = std::deque<T>>
class BlockingQueue {
public:
  explicit BlockingQueue(const size_t& capacity);
  void Put(T&& element);
  bool Get(T& result);
  void Shutdown();

private:
  std::condition_variable empty;
  std::condition_variable overflow;
  Container data_queue;
  std::mutex mutex_;
  const size_t capacity_;
  std::atomic_bool shut_down;
};

template <class T, class Container = std::deque<T>>
BlockingQueue<T, Container>::BlockingQueue(const size_t& capacity): 
  capacity_(capacity),
  shut_down(false) {}

template <class T, class Container = std::deque<T> >
void BlockingQueue<T, Container>::Put(T&& element) {
  if (shut_down) {
    throw std::logic_error("Method shut down was invoked");
  }
  std::unique_lock<std::mutex> locker(mutex_);
  //Wait until some thread puts something in the queue or calls the shutdown method
  overflow.wait(locker, [this]() {return (data_queue.size() < capacity_) || (shut_down); });

  if (shut_down) {
    throw std::logic_error("Method shut down was invoked");
  }

  data_queue.push_back(std::move(element));
  empty.notify_all();
}

template <class T, class Container = std::deque<T>>
bool BlockingQueue<T, Container>::Get(T& result) {
  std::unique_lock<std::mutex> locker(mutex_);
  //Wait until some thread puts something in the queue or calls the shutdown method.
  empty.wait(locker, [this]() {return (data_queue.size() > 0) || (shut_down); });
  //If shutdown was called, but the queue is empty, then return false.
  //Otherwise, extract the element from the queue.
  if (shut_down && (data_queue.size() == 0)) {
    return false;
  }
  result = std::move(data_queue.front());
  data_queue.pop_front();
  overflow.notify_all();
  return true;
}

template <class T, class Container = std::deque<T>>
void BlockingQueue<T, Container>::Shutdown() {
  shut_down = true;
}
