#include <iostream>
#include <atomic>
#include <thread>
#include <vector>
#include <array>


class PetersonMutex {
public:
  PetersonMutex();
  PetersonMutex(const PetersonMutex&) {}
  void lock(int cur_id);
  void unlock(int cur_id);

private:
  std::array<std::atomic<bool>, 2> want;
  std::atomic<int> victim;
};

class TreeMutex {
public:
  TreeMutex(std::size_t num_threads);
  void lock(std::size_t current_thread);
  void unlock(std::size_t current_thread);

private:
  std::vector<PetersonMutex> tree;
  std::size_t num_mutexes;
};

PetersonMutex::PetersonMutex() {
  victim.store(0);
  want[0].store(false);
  want[1].store(false);
}

void PetersonMutex::lock(int cur_id) {
  want[cur_id].store(true);
  victim.store(cur_id);
  while (want[1 - cur_id].load() && victim.load() == cur_id) {
    std::this_thread::yield();
  }
}

void PetersonMutex::unlock(int cur_id) {
  want[cur_id].store(false);
}

TreeMutex::TreeMutex(std::size_t num_threads) {
  num_mutexes = 1;
  while (num_mutexes < num_threads) {
    num_mutexes *= 2;
  }
  tree.resize(num_mutexes);
}

void TreeMutex::lock(std::size_t current_thread) {
  std::size_t child_index = current_thread + num_mutexes - 1;
  while (child_index > 0) {
    std::size_t parent_mutex_index = (child_index - 1) / 2;
    tree[parent_mutex_index].lock((child_index + 1) % 2);
    child_index = parent_mutex_index;
  }
}

void TreeMutex::unlock(std::size_t current_thread) {
  int path = num_mutexes;
  int current_choise = 0;
  std::size_t child_mutex_index = 0;
  while (path != 0 && child_mutex_index < num_mutexes) {
    path /= 2;
    if ((path & current_thread) == 0)
      current_choise = 0;
    else
      current_choise = 1;
    tree[child_mutex_index].unlock(current_choise);
    child_mutex_index = child_mutex_index * 2 + 1 + current_choise;
  }
}